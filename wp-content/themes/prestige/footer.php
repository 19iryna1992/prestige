<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package pkp
 */

if ( ! defined( 'ABSPATH' ) ) exit;

?>

</div><!-- #content -->

<footer class="o-footer">
     <?php get_template_part('template-parts/organisms/footer/main-footer'); ?>
</footer>


</div><!-- #page -->
<?php wp_footer(); ?>

</body>
</html>
