<?php

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package pkp
 */

if (!defined('ABSPATH')) exit;

get_header();

//ACF

$post_excerpt = get_field('post_excerpt');

?>

    <main id="main" role="main" tabindex="-1">
        <section class="s-blog-posts">
            <div class="s-info-boxes__intro">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h2 class="c-intro-title text-center">BLOG</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row justify-content-center">
                    <?php while (have_posts()) : the_post(); ?>
                        <div class="col-12 col-lg-10">
                            <a href="<?php echo get_the_permalink()?>">
                                <div class="c-blog-post">
                                    <div class="c-blog-post__title">
                                       <h2 class="c-intro-title c-intro-title--secondary"> <?php the_title(); ?> </h2>
                                    </div>
                                    <?php if ($post_excerpt): ?>
                                        <div class="c-blog-post__excerpt c-intro-description c-intro-description--light u-color-text">
                                            <?php echo $post_excerpt ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </a>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </section>
    </main>

<?php

get_footer();

?>