<?php

if (!defined('ABSPATH')) exit;

/** *
 *
 * Function for getting svg icons
 *
 */

function get_check_icon()
{
    return '<svg width="35" height="32" viewBox="0 0 35 32" fill="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<rect width="35" height="32" fill="url(#check-icon)"/>
<defs>
<pattern id="check-icon" patternContentUnits="objectBoundingBox" width="1" height="1">
<use xlink:href="#image-check-icon" transform="translate(0.0428571) scale(0.0136461 0.0149254)"/>
</pattern>
<image id="image-check-icon" width="67" height="67" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEMAAABDCAYAAADHyrhzAAAACXBIWXMAABcSAAAXEgFnn9JSAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA75JREFUeNrsW4uNozAQhegKSAmUQAfLdkAJpILLdUAHlMB1QAlsB2QrIFcB6YAz2slpxXnswR7/pIxkRZu1PfbLzHg+dpa96EUyyn0wWdf1LD4q0UrR3uDrCun+EO0m2l20P6J95Hn+kTTKAoBCtFa0aeWhQbQGgE0GhG3B4+qWetHK2EGYV780coKSM4CwLaYHe0Chpz34VPTZ7EoBjUK/RfslbMsjpDR0hF9vhn61od1pwF7oaDHhwWUcJ4LBqzhPJKIqdj6BKOFXUIFQBLZPo/NTBxaxKNSh8iyhV8V6JmeAABCoaIY6/zUqyw+IQjW275pIjvbeOSCAPAZEVI4PqI3UjnExmFIAgqDOrQs/YonaFVb7P5XphBUyYZMlQIijNnOqR5clQuCkzdbqgujdnFT4jEv3cmgfCKJVliAh9qO1kYohUjVodAkf6LcYSQeSmCkiBOK7TZs0/VvJnq4UB+u/rFLkQGjVGJGOKTNAsIoMDMztLgzGlUeO0zkRIDrC2JKsKiBK0foVCiD6A3PsT8kR61hLGNWRANHZAoEBSrYXkQdePdNcJeVInSIGwihHgdiNf9J/Uoy9hwYi+ypB7GkrNbyblAXEmJvk61IGxv4I/QwZU3ADQfmRT0yLr0HNBttcB4yXuf8PBiBkYDwL4dkPBiD2i98ixXdEJClzbcfd2REQSlJJBpXxWfL34boFAYiba/U8HdjkER08BAj0G0ICwWIzxEI3MC6IldYCAv/fJEIWW1w8APGQLWqxSY+Z+ASKCNRZrpW0T4nTNTI6SUMMQADflQJGzxGxUuMIReDlEoiKlKJAchmFIVNlhMkRgRqu60raI4JaY8G4V1Tr1xAZNUktZTmiT4Ml8556Yc1TupC+PxlytrUSyg2fgBFwwzfALoHr9kKJ/rRclbwRUZqZRHQKCERhJJGInjdMgIze7lzp91RTBpYupCNgbsRuP4h+XRMFY7SSdATNJbYyo6GTNXPp2ZS4ephVCJH6ZHS1V8XaZ1afBiksRW0/FH6NtQOpikKbhIDgK6ArHtN0iQDR+GI0hL7rBcbSXzSsAcT7JXrN8ene2GsA8XqhHqRhDJkWeAIyaF4Eta5AgaBLlydpfItnS3gmxfLK8NvNvpHAs4rRcO1tSg8bKolzV2APBmK2zNqQ51xSIj5+ZvQq3Eb3TF6NKw3muUT1KhpEudW8X+OkOfpL/QDKlfHZt0wd6iw1AptytXwOvvh6B5/7Bif7KjA/jejbrstWBH7eGNpswB0K2y96UUD6K8AA/CJJoJawOsMAAAAASUVORK5CYII="/>
</defs>
</svg>';
}

function get_check_icon_mobile()
{
    return '<svg width="35" height="32" viewBox="0 0 35 32" fill="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<rect width="35" height="32" fill="url(#check-icon-m)"/>
<defs>
<pattern id="check-icon-m" patternContentUnits="objectBoundingBox" width="1" height="1">
<use xlink:href="#image-check-icon-m" transform="translate(0.0428571) scale(0.0136461 0.0149254)"/>
</pattern>
<image id="image-check-icon-m" width="67" height="67" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEMAAABDCAYAAADHyrhzAAAACXBIWXMAABcSAAAXEgFnn9JSAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA75JREFUeNrsW4uNozAQhegKSAmUQAfLdkAJpILLdUAHlMB1QAlsB2QrIFcB6YAz2slpxXnswR7/pIxkRZu1PfbLzHg+dpa96EUyyn0wWdf1LD4q0UrR3uDrCun+EO0m2l20P6J95Hn+kTTKAoBCtFa0aeWhQbQGgE0GhG3B4+qWetHK2EGYV780coKSM4CwLaYHe0Chpz34VPTZ7EoBjUK/RfslbMsjpDR0hF9vhn61od1pwF7oaDHhwWUcJ4LBqzhPJKIqdj6BKOFXUIFQBLZPo/NTBxaxKNSh8iyhV8V6JmeAABCoaIY6/zUqyw+IQjW275pIjvbeOSCAPAZEVI4PqI3UjnExmFIAgqDOrQs/YonaFVb7P5XphBUyYZMlQIijNnOqR5clQuCkzdbqgujdnFT4jEv3cmgfCKJVliAh9qO1kYohUjVodAkf6LcYSQeSmCkiBOK7TZs0/VvJnq4UB+u/rFLkQGjVGJGOKTNAsIoMDMztLgzGlUeO0zkRIDrC2JKsKiBK0foVCiD6A3PsT8kR61hLGNWRANHZAoEBSrYXkQdePdNcJeVInSIGwihHgdiNf9J/Uoy9hwYi+ypB7GkrNbyblAXEmJvk61IGxv4I/QwZU3ADQfmRT0yLr0HNBttcB4yXuf8PBiBkYDwL4dkPBiD2i98ixXdEJClzbcfd2REQSlJJBpXxWfL34boFAYiba/U8HdjkER08BAj0G0ICwWIzxEI3MC6IldYCAv/fJEIWW1w8APGQLWqxSY+Z+ASKCNRZrpW0T4nTNTI6SUMMQADflQJGzxGxUuMIReDlEoiKlKJAchmFIVNlhMkRgRqu60raI4JaY8G4V1Tr1xAZNUktZTmiT4Ml8556Yc1TupC+PxlytrUSyg2fgBFwwzfALoHr9kKJ/rRclbwRUZqZRHQKCERhJJGInjdMgIze7lzp91RTBpYupCNgbsRuP4h+XRMFY7SSdATNJbYyo6GTNXPp2ZS4ephVCJH6ZHS1V8XaZ1afBiksRW0/FH6NtQOpikKbhIDgK6ArHtN0iQDR+GI0hL7rBcbSXzSsAcT7JXrN8ene2GsA8XqhHqRhDJkWeAIyaF4Eta5AgaBLlydpfItnS3gmxfLK8NvNvpHAs4rRcO1tSg8bKolzV2APBmK2zNqQ51xSIj5+ZvQq3Eb3TF6NKw3muUT1KhpEudW8X+OkOfpL/QDKlfHZt0wd6iw1AptytXwOvvh6B5/7Bif7KjA/jejbrstWBH7eGNpswB0K2y96UUD6K8AA/CJJoJawOsMAAAAASUVORK5CYII="/>
</defs>
</svg>';
}


function get_phone_icon()
{
    return '<svg width="35" height="35" viewBox="0 0 35 35" fill="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<rect width="35" height="35" fill="url(#phone-icon)"/>
<defs>
<pattern id="phone-icon" patternContentUnits="objectBoundingBox" width="1" height="1">
<use xlink:href="#image-phone-icon" transform="scale(0.01)"/>
</pattern>
<image id="image-phone-icon" width="100" height="100" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABZ1JREFUeNrsXeuRmzAQxp78DyWQCsJVEK6CkArCdeB0QCrgOuBSwbkDLhXgVCCnArsDIubWGY6ThBB6wu6Mxne2R4/9tA+tlnUUISEhISEhIVmlruti5II/YBS0XWhLkRt+gHEjBMUjMIyAsps5oYS+5LT1+vOL4pgn2v7S9rLb7U6BAdIzvoH1D+lK27219dCJZLQ1nX4i/a4LDRSQCqOSIppA1ZmnJiSvxRkotPPnzh61CIrAhtBOa/rCUydXsAUqlEBj2heqh+/QprBtBm8X55p2F0/6SrQp7wdhGfDaohsZIyhvO39ndC07DUF5XkZB6VUGo9PMZEyIsZDnQA+O+kFh6HZiYSHjMduAT/OLQNkz3hvr77OFdfwZ/R9sfAg8q3vwtMZ8baZA2XuyjhPHpdwcKB8Y750nJMYEnTnnlZMBlZJZxOYnbRUHFLlzCsuoW9K9Y6osjeOKmDZlL7NbIcprml7WYkckiam+9jPUh207km3gmjQe83YvY2At7dbfjPeylQPyQO3IUWjU6ReudGdeR8b8swOV1dNX2o6ax7m3yPCUYdSHYDypxrKsHNQgeGn1UOrggDg/NOTQ0zowJp9uGgzoMLcZzxp6czaizEGBIWBM6UhtXTYNxqBz4iICG6raMgoGDFA7siOZC3XpCxj7OecCS4y5hmYzIva9+jzXVgIQ3rnAxul1k2B4eS7gXOnGPoJh1GbMYE5icJGs61yCYLwdeEwHg2C0vqcFOQND4P4Si2B4lRLkHAyB2kotgOFdOpAgz7mwvSuM3OZNgOFlyIRxPitcTILoDmeECAYDlMLVBA46xTRkMEZnEGeDJ7rSS9cAhq+6UynGxLFJCIZE6GRMvxjvfZ87IOQiPYUew/JFSlodJ3dQgZfQoro+AlLoUjWcLHuCFRKWu8DK8S2OxFXIZXdSwjPwOXJ6uZSkin2VnBhWgpxeJiXNgv5YqqtFTi+XklyxL14UFc8mM5iY6fSSOFJn7P5lraA0Oi+UONGAIJ/KdQVIwmFgqhlkPDQu9JLaBf2Jbg+xWJgkA4lm1eW2BNJKDfxS1SUCBdWXBAMr3SWXbCUVwDgZqN9sFYALVFetgVkX3S4xOCS1oO8OPs/XqLoKg6DUCv2VCo8w13BWSkIDpdRtTyRAaWUYBVKso3YkAYAOsAlj30FpTCS9gZppBbs4V3CndRYCaACk2DdAYs5ubjWAMrXLq/EYE2AQYGI6AL3QIEmlV8CYTmaYqJZKbp7SVJaLiGkAzmFBMdDWN1AKw6DkEl5Sq9EZuLnGzcS4XoNSGQYlUbANOsfOQIpELnTjm03hRXErjWOUNsEQ2Df3CdmSk21Nl4UFu9W6AENCVbe+AWIthRRUyMXl7SPHS0u2DEo8UJW1g7VmQVy0LXFDVQ2vw7WGUbV7ApSgivLPtJuTgDipStrX5Ipe61bxiqW1K7mImp1A7qxMbA8K/BLCE+PjXs00W0xucF63l4LywAGlV1u1CbuCgMiB8sD5uIgkKkIjIPpB6aXkjqN3b3blgIDYBaU38p8ifkXrCg5cKQJi39g/cr6SgbSUEZKT0y6RuffwdP5NEOeQGdLyAnaFV7v35h43a3meZO/7BEGFfaN/9u0sUGO3xIMEAbEDzBGk5VHwtWItwIRmW1LJJITacXCxCSK4qHHBxYTRH/60a+5gfmRTgAwWfpBMOiBw1ZtYmle3SUBg8TEwe042yMEUOJxy7dt83HuGKhuDo7NSXhtCdVXbwOQKGYhkkFit+uw9K8vmOUL6z6AE0nNIN59uObslgJRNjLPoGcndFqUmeq3Q3b8uUSFnaNdB1IAnUY/0HPUDRUNOpdWKkiNLDXJa/bB5S6y+aAIDK1EYAKhWyB0mqi7uDlk/DySwO72B/siwGf1PfBylflIVCQkJCQlJM/0TYAAYNZQTuDkdVgAAAABJRU5ErkJggg=="/>
</defs>
</svg>
';
}

function get_close_icon()
{
    return  '<svg version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 20.1 20.2" style="enable-background:new 0 0 20.1 20.2;" xml:space="preserve">
	<path  d="M19.6,0.5L0.5,19.7 M19.6,19.6L0.5,0.5" fill="none" stroke-linecap="round" stroke-linejoin="round" style="stroke:currentColor;"/>
</svg>';

}