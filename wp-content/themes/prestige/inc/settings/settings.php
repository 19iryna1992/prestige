<?php

if ( ! defined( 'ABSPATH' ) ) exit;

require_once __DIR__ . '/cleanup.php';
require_once __DIR__ . '/support.php';
require_once __DIR__ . '/widgets.php';
require_once __DIR__ . '/styles-scripts.php';
require_once __DIR__ . '/comments.php';

