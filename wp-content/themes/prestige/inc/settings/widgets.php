<?php

if (!defined('ABSPATH')) exit;

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function pkp_widgets_init()
{

    register_sidebar(array(
        'name' => esc_html__('Footer Column 1', 'pkp'),
        'id' => 'footer-1',
        'description' => esc_html__('Add widgets here.', 'pkp'),
        'before_widget' => '<section id="%1$s" class="c-widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="c-widget__title">',
        'after_title' => '</h2>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Footer Column 2', 'pkp'),
        'id' => 'footer-2',
        'description' => esc_html__('Add widgets here.', 'pkp'),
        'before_widget' => '<section id="%1$s" class="c-widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="c-widget__title">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'pkp_widgets_init');