<?php

if (!defined('ABSPATH')) exit;

/**
 * https://typerocket.com/ultimate-guide-to-custom-post-types-in-wordpress/
 * 
 * Flush Rewrites
 */
add_action('after_switch_theme', 'pkp_flush_rewrite_rules');

function pkp_flush_rewrite_rules()
{
  flush_rewrite_rules();
}

require_once 'post-type-labels.php';
//require_once 'example.php';
