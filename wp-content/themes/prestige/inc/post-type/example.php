<?php

if (!defined('ABSPATH')) exit;

add_action('init', function () {

    $labels = pkp_post_type_labels('Example', 'Examples');

    $args = array(
        'description' => __('Example', 'pkp'),
        'labels' => $labels,
        'supports' => ['title', 'editor', 'thumbnail', 'revisions'],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_icon'     => 'dashicons-portfolio',
        'menu_position' => 20,
        'rewrite' => ['slug' => 'example', 'with_front' => false],
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type'    => 'post',
    );

    register_post_type('example', $args);
});

add_action('init', function () {

    $tax =  'example_services';
    $type = ['example'];

    $labels = pkp_post_type_labels('Example Service', 'Example Services');

    $args = [
        'description'         => 'Services',
        'labels'              => $labels,
        'hierarchical'        => true,
        'show_ui'             => true,
        'show_admin_column'   => true,
        'show_in_quick_edit'  => true,
        'show_in_menu'        => true,
        'rewrite'             => ['slug' => 'services', 'with_front' => false],
    ];

    register_taxonomy($tax, $type, $args);
});

add_action('init', function () {

    $tax =  'exmple_industries';
    $type = ['exmple'];

    $labels = pkp_post_type_labels('Example Industry', 'Example Industries');

    $args = [
        'description'         => 'Services',
        'labels'              => $labels,
        'hierarchical'        => true,
        'show_ui'             => true,
        'show_admin_column'   => true,
        'show_in_quick_edit'  => true,
        'show_in_menu'        => true,
        'rewrite'             => ['slug' => 'industry', 'with_front' => false],
    ];

    register_taxonomy($tax, $type, $args);
});
