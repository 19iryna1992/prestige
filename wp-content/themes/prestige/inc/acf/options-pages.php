<?php

if (!defined('ABSPATH')) exit;

/**
 * Create Global Elements Menu Item
 */
if (function_exists('acf_add_options_page')) {

	# Site Globals (Parent)
	$site_globals = acf_add_options_page(array(
		'page_title' 	=> 'Options',
		'menu_title' 	=> 'Options',
		'icon_url'		=> 'dashicons-admin-generic',
		'redirect' 		=> true
	));

	# Contacts
	$page_contacts = acf_add_options_sub_page(array(
		'page_title'  => 'Contacts & Info',
		'menu_title'  => 'Contacts',
		'menu_slug'   => 'contacts',
		'position'    =>  '1',
		'parent_slug' 	=> $site_globals['menu_slug'],
	));

    # Header
    $page_header = acf_add_options_sub_page(array(
        'page_title'  => 'Header',
        'menu_title'  => 'Header',
        'menu_slug'   => 'header',
        'position'    =>  '2',
        'parent_slug' 	=> $site_globals['menu_slug'],
    ));

	# JS Tracking
	$page_tracking_scripts = acf_add_options_page(array(
		'page_title'  => 'Tracking Scripts',
		'menu_title'  => 'Tracking Scripts',
		'menu_slug'   => 'tracking',
		'position'    =>  '6',
		'parent_slug' 	=> $site_globals['menu_slug'],
	));
}
