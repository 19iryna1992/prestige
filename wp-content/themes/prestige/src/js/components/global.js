$(document).ready(function () {

    var mobileMenuBtn = $('.JS--mobile-btn');
    var mainMenu = $('.JS--main-menu');

    mobileMenuBtn.on('click', function () {
        $(this).toggleClass('is-open');
        mainMenu.slideToggle(300);
    });


    var partnersFront = $('.JS--partner-front');
    if (partnersFront.length !== 0) {

        partnersFront.on('click', function () {
            if ($(window).width() < 1024) {
                partnersFront.not($(this)).next('.JS--partner-back').fadeOut();
                $(this).next('.JS--partner-back').fadeIn(300);
            }
        });

        $('.JS--close-partner').on('click', function () {
            $(this).closest('.JS--partner-back').fadeOut();
        });

    }

});