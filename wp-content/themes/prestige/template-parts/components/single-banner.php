<?php

//vars
$banner_image = get_field('home_banner');

?>

<div class="c-single-banner u-bg-img" style="background-image: url('<?php echo $banner_image['url']; ?>')"></div>