<?php
//vars
$facebook = get_field('company_facebook', 'option');
?>

<div class="c-socials-menu">
    <ul class="c-socials-menu__list">
        <?php if ($facebook) : ?>
            <li class="c-socials-menu__item">
                <a href="<?php echo $facebook; ?>" target="_blank" class="c-socials-menu__link">
                    <i class="fab fa-facebook-f"></i>
                </a>
            </li>
        <?php endif; ?>
    </ul>
</div>