<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

?>
<div class="c-main-menu">
    <?php
    wp_nav_menu( array(
        'theme_location'  => 'primary-menu',
        'menu_id'         => 'primary-menu',
        'container_class' => 'c-main-menu__container',
        'menu_class'      => 'c-main-menu__list',
    ) );
    ?>
</div>