<?php

//vars

$form_shortcode = $args['form_shortcode'];

?>
<?php if ($form_shortcode) : ?>
    <div class="c-form">
        <?php echo do_shortcode($form_shortcode); ?>
    </div>
<?php endif; ?>
