<?php
//vars

$first_phone = get_field('header_first_phone', 'option');
$second_phone = get_field('header_second_phone', 'option');
$button = get_field('header_button', 'option');
?>
<div class="c-header-cta">
    <?php if ($first_phone || $second_phone) : ?>
    <div class="c-header-cta__phone">
        <div class="c-header-cta__phone-icon">
            <?php echo get_phone_icon(); ?>
        </div>
        <div class="c-header-cta__phone-links">
            <?php if ($first_phone) : ?>
                <a href="tel:<?php echo str_replace(array('(', ')', '-', ' '), '', $first_phone); ?>"><?php echo $first_phone; ?></a>
            <?php endif; ?>
            <?php if ($second_phone) : ?>
                <br>
                <a href="tel:<?php echo str_replace(array('(', ')', '-', ' '), '', $second_phone); ?>"><?php echo $second_phone; ?></a>
            <?php endif; ?>
        </div>

    </div>
    <?php endif; ?>
    <?php if ($button) : ?>
        <div class="c-header-cta__btn">
            <a href="<?php echo $button['url']; ?>" class="c-button c-button--primary c-button--icon">
                <span class="c-button__icon"><?php echo get_check_icon(); ?></span>
                <span class="c-button__icon c-button__icon--mobile"><?php echo get_check_icon_mobile(); ?></span>
                <?php echo $button['title']; ?>
            </a>
        </div>
    <?php endif; ?>
</div>