<?php
//vars
$title = get_field('zomow_doc_title');
$form = get_field('zomow_doc_form');
?>

<section class="s-document-form">
    <?php if ($title) : ?>
        <div class="s-document-form__intro">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2 class="c-intro-title text-center"><?php echo $title; ?></h2>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if ($form) : ?>
        <div class="s-document-form__form">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-10">
                        <?php get_template_part('template-parts/components/form', null, ['form_shortcode' => $form]); ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

</section>