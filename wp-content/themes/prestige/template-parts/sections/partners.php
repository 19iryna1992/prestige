<?php

//vars
$title = get_field('partners_title');

?>
<?php if (have_rows('partners')): ?>
    <section class="s-partners">
        <?php if ($title) : ?>
            <div class="s-partners__intro">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h2 class="c-intro-title text-center"><?php echo $title; ?></h2>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="c-partner-grid">
                        <?php while (have_rows('partners')): the_row();
                            $logo = get_sub_field('partner_logo');
                            $title = get_sub_field('partner_title');
                            $description = get_sub_field('partner_description');
                            ?>
                            <div class="c-partner-grid__column">
                                <div class="c-partner">
                                    <div class="c-partner__front JS--partner-front">
                                        <div class="c-partner__logo">
                                            <?php if ($logo) : ?>
                                                <?php echo wp_get_attachment_image($logo['ID'], 'full'); ?>
                                            <?php else : ?>
                                                <h2 class="c-intro-title"><?php _e('PARTNER', 'pkp'); ?></h2>
                                            <?php endif; ?>

                                        </div>
                                        <div class="c-partner__header">
                                            <h3 class="c-partner__title text-center"><?php echo $title; ?></h3>
                                        </div>
                                    </div>
                                    <?php if ($description) : ?>
                                        <div class="c-partner__back JS--partner-back">
                                            <span class="c-partner__close JS--close-partner">
                                                <?php echo get_close_icon(); ?>
                                            </span>
                                            <h3 class="c-partner__title"><?php echo $title; ?></h3>
                                            <div class="c-partner__description">
                                                <?php echo $description; ?>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>