<?php
//title
$section_title = get_field('info_boxes_title');
?>

<?php if (have_rows('info_boxes')): ?>
    <section class="s-info-boxes">
        <?php if ($section_title) : ?>
            <div class="s-info-boxes__intro">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h2 class="c-intro-title text-center"><?php echo $section_title; ?></h2>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>


        <div class="container">
            <div class="row no-gutters">
                <div class="col-md-6 col-lg-4">
                    <?php while (have_rows('info_boxes')):
                    the_row();
                    $last_item = get_sub_field('info_box_last_item');
                    $title = get_sub_field('info_box_title');
                    $content = get_sub_field('info_box_content');
                    ?>
                    <div class="c-info-box">
                        <?php if ($title) : ?>
                            <h2 class="c-intro-title c-intro-title--secondary"><?php echo $title; ?></h2>
                        <?php endif; ?>
                        <?php if ($content) : ?>
                            <div class="c-intro-description c-intro-description--light u-color-text">
                                <?php echo $content; ?>
                            </div>
                        <?php endif; ?>
                    </div>


                    <?php if ($last_item): ?>
                </div>
                <div class="col-md-6 col-lg-4">
                    <?php endif; ?>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>

    </section>
<?php endif; ?>









