<?php

//vars
$title = get_field('contact_info_title');
$big_description = get_field('contact_info_big_description');
$small_description = get_field('contact_info_small_description');
?>

<section class="s-contact-info u-bg-secondary">
    <div class="s-contact-info__intro">
        <div class="container">
            <div class="row justify-content-center">
                <?php if ($title) : ?>
                    <div class="col-12">
                        <h2 class="c-intro-title text-center"><?php echo $title; ?></h2>
                    </div>
                <?php endif; ?>
                <?php if ($big_description) : ?>
                    <div class="col-md-5 col-xl-4">
                        <div class="s-contact-info__description u-white">
                            <?php echo $big_description; ?>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if ($small_description) : ?>
                    <div class="col-md-7 col-xl-8">
                        <div class="c-intro-description c-intro-description--small">
                            <?php echo $small_description; ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <?php if (have_rows('contact_adressess')): ?>
        <div class="c-addresses">

            <div class="container">
                <?php
                $address_counter = 1;
                while (have_rows('contact_adressess')): the_row();
                    $address = get_sub_field('address_info');
                    $map_img = get_sub_field('map_image');
                    $map_link = get_sub_field('map_link');
                    ?>
                    <div class="row">
                        <div class="col-md-5 col-xl-4">
                            <div class="c-addresses__content text-center">
                                <span class="c-address__number">
                                    <?php echo $address_counter; ?>
                                </span>
                                <div class="c-intro-description c-intro-description--small">
                                    <?php echo $address; ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7 col-xl-8">
                            <a href="<?php echo $map_link; ?>" target="_blank" class="c-addresses__map <?php echo ($address_counter == 1) ? 'c-addresses__map--first' : null; ?>">
                                <?php
                                if ($map_img) {
                                    echo wp_get_attachment_image($map_img['ID'], 'full');
                                }else{
                                    echo '<img src="https://fakeimg.pl/532x187/?text=Map">';
                                }
                                ?>
                            </a>
                        </div>
                    </div>
                <?php $address_counter++; endwhile; ?>
            </div>
        </div>
    <?php endif; ?>

</section>











