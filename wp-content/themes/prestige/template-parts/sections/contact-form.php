<?php
//vars

$title = get_field('contact_title');
$form = get_field('contact_form_shortcode');
?>
<?php if ($form) : ?>
    <section class="s-contact-form">
        <div class="container">
            <div class="row">
                <?php if ($title) : ?>
                    <div class="col-12">
                        <div class="s-contact-form__intro">
                            <h2 class="c-intro-title text-center"><?php echo $title; ?></h2>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="col-12">
                    <div class="s-contact-form__form">
                        <?php get_template_part('template-parts/components/form', null, ['form_shortcode' => $form]); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>