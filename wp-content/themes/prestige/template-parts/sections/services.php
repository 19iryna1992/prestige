<?php

$services_flag_title = get_field('services_flag_title');
$small_flags_description = get_field('services_small_description');
$button = get_field('services_button');

$polski_srvices_title = get_field('polski_srvices_title');
$services_left_column = get_field('services_left_column');
$services_right_column = get_field('services_right_column');
?>

<section class="s-services u-bg-secondary">

    <div class="s-services__flags">
        <div class="container">
            <div class="row">
                <?php if ($services_flag_title) : ?>
                    <div class="col-12">
                        <h2 class="c-intro-title text-center">
                            <?php echo $services_flag_title; ?>
                        </h2>
                    </div>
                <?php endif; ?>
                <div class="col-12">
                    <?php if (have_rows('services_flags')): ?>
                        <div class="c-flag">
                            <?php while (have_rows('services_flags')): the_row();
                                $flag = get_sub_field('flag');
                                ?>
                                <div class="c-flag__img">
                                    <?php if ($flag) : ?>
                                        <?php echo wp_get_attachment_image($flag['ID'], 'flag-thumb'); ?>
                                    <?php else : ?>
                                        <img src="https://fakeimg.pl/100x70/?text=Flag" alt="flag">
                                    <?php endif; ?>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <div class="s-services__zagran-desc">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <div class="c-intro-description">
                        <?php echo $small_flags_description; ?>
                    </div>
                </div>
                <div class="col-md-6 text-center text-md-right">
                    <?php if ($button) : ?>
                        <a href="<?php echo $button['url']; ?>" target="<?php echo $button['target']; ?>"
                           class="c-button c-button--primary"><?php echo $button['title']; ?></a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <div class="s-services__polski">
        <div class="container">
            <div class="row justify-content-center">
                <?php if ($polski_srvices_title) : ?>
                    <div class="col-12">
                        <h2 class="c-intro-title text-center">
                            <?php echo $polski_srvices_title; ?>
                        </h2>
                    </div>
                <?php endif; ?>
                <div class="<?php echo ($services_right_column) ? 'col-md-6' : 'col-12'; ?>">
                    <?php if ($services_left_column) : ?>
                        <div class="c-intro-description s-services__polski--first">
                            <?php echo $services_left_column; ?>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="<?php echo ($services_left_column) ? 'col-md-6' : 'col-12'; ?>">
                    <?php if ($services_right_column) : ?>
                        <div class="c-intro-description">
                            <?php echo $services_right_column; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

</section>