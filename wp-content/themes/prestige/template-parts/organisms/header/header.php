<?php

if (!defined('ABSPATH')) exit;

?>

<?php if (is_front_page()) : ?>
    <div class="o-header u-bg-secondary">
        <div class="o-header__top">
            <?php get_template_part('template-parts/components/header-cta'); ?>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">

                </div>
                <div class="col-12">
                    <div class="o-header__logo">
                        <?php the_custom_logo(); ?>
                        <div class="o-header__mobile-btn JS--mobile-btn">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="o-header__menu JS--main-menu">
                        <?php get_template_part('template-parts/components/navigation/main-menu'); ?>
                        <?php get_template_part('template-parts/components/header-cta'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php else : ?>

    <div class="o-header o-header--inline u-bg-secondary">
        <div class="o-header__top">
            <?php get_template_part('template-parts/components/header-cta'); ?>
        </div>
        <div class="container">
            <div class="row align-items-baseline">
                <div class="col-12">

                </div>
                <div class="col-auto o-header__logo-column">
                    <div class="o-header__logo o-header__logo--left">
                        <?php the_custom_logo(); ?>
                        <div class="o-header__mobile-btn JS--mobile-btn">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="o-header__menu JS--main-menu">
                        <?php get_template_part('template-parts/components/navigation/main-menu'); ?>
                        <?php get_template_part('template-parts/components/header-cta'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php endif; ?>
