<?php

if (!defined('ABSPATH')) exit;


?>

<div class="container">
    <div class="row justify-content-between">
        <div class="col-12 order-1">
            <div class="o-footer__divider"></div>
        </div>
        <div class="col-md-6 order-3 order-md-2">
            <div class="o-footer__address">
                <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-column-1')) : endif; ?>
            </div>
        </div>
        <div class="col-md-6 col-lg-5 order-2 order-md-3">
            <div class="o-footer__newsletter">
                <div class="o-footer__social">
                    <?php get_template_part('template-parts/components/navigation/socials-menu'); ?>
                </div>
                <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-column-2')) : endif; ?>
            </div>
        </div>
    </div>
</div>
