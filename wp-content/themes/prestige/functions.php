<?php

/**
 * pkp functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package pkp
 */

if (!defined('ABSPATH')) exit;

require_once __DIR__ . '/inc/settings/settings.php';
require_once __DIR__ . '/inc/acf/acf.php';
require_once __DIR__ . '/inc/admin/admin.php';
require_once __DIR__ . '/inc/utils/utils.php';
require_once __DIR__ . '/inc/post-type/post-type.php';
require_once __DIR__ . '/inc/posts/posts.php';
require_once __DIR__ . '/inc/plugins/plugins.php';
