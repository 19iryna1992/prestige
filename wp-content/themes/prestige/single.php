<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package pkp
 */

if (!defined('ABSPATH')) exit;

get_header();

while (have_posts()) : the_post();


    ?>

    <main id="main" role="main" tabindex="-1">
        <article class="o-single-post">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="o-single-post__title">
                            <h2 class="c-intro-title c-intro-title--secondary"> <?php the_title(); ?> </h2>
                        </div>
                        <div class="c-wysiwyg">
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </main>

<?php endwhile; ?>

<?php get_footer(); ?>