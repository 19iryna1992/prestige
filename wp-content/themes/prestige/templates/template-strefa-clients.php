<?php /* Template Name: Strefa klienta */

if (!defined('ABSPATH')) exit;

get_header(); ?>

    <main id="main" role="main" tabindex="-1">

        <?php get_template_part('template-parts/sections/info-boxes'); ?>
        <?php get_template_part('template-parts/sections/contact-form'); ?>

    </main>

<?php get_footer(); ?>