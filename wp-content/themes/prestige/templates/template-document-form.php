<?php /* Template Name: Zamov document */

if (!defined('ABSPATH')) exit;

get_header(); ?>

    <main id="main" role="main" tabindex="-1">

        <?php get_template_part('template-parts/sections/document-form'); ?>

    </main>

<?php get_footer(); ?>