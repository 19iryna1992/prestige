<?php /* Template Name: Home */

if (!defined('ABSPATH')) exit;

get_header(); ?>

    <main id="main" role="main" tabindex="-1">

        <?php get_template_part('template-parts/components/single-banner'); ?>
        <?php get_template_part('template-parts/sections/services'); ?>
        <?php get_template_part('template-parts/sections/contact-form'); ?>
        <?php get_template_part('template-parts/sections/partners'); ?>

    </main>

<?php get_footer(); ?>